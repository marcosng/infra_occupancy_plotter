# **ROOM OCCUPANCY PLOTTER**
***
## DESCRIPCION GENERAL
Ploteo de ocupacion de *room* de sitios.
***
## DESCRIPCION DETALLADA
* **main.py**  
Archivo principal. Llamado a funciones.
* **svg_func.py**  
Archivo de funciones  
  
***
## INPUT FILE
1. Ingresar a netcom.ldc.efn.uncor.edu
2. Manager -> Seleccionar cualquier sitio
3. Rooms -> Seleccionar alguna room
4. Action: Query space usage
***
## LIBRER�AS NECESARIAS
svgwrite
openpyxl
