import os, glob
import svg_funcs
from svg_funcs import *
import openpyxl

##############################
#       FUNCIONES
##############################
def get_files():
    files = []
    for file in glob.glob("*xlsx"):
        files.append( file )
    return files

def get_data( file_name ):
    try:
        file = openpyxl.load_workbook( file_name )
        sheet = file.get_sheet_by_name( "Tablib Dataset" )

        r = []
        u = []
        f = []
        u_n = {}
        f_n = {}
        for n in range(2, sheet.max_row+1):
            # Lista de filas
            r.append( sheet.cell(row=n, column=1).value )

            # Lista de posiciones ocupadas
            aux1 = sheet.cell(row=n, column=2).value.split("\n")
            u_n[sheet.cell(row=n, column=1).value] = len(aux1)
            for m in aux1:
                u.append( m.split(" ")[0] )   

            # Lista de posiciones vacías
            aux2 = sheet.cell(row=n, column=3).value.split(", ")
            f_n[sheet.cell(row=n, column=1).value] = len(aux2)
            for m in aux2:
                f.append( m )
                
    except FileNotFoundError:
        print("No se encontró el archivo especificado.")
        r = None
        u = None
        f = None   
            
    return r, u, u_n, f, f_n


def main(file_name):
    # Filas, posiciones ocupadas, libres y totales
    rows, occ, occ_n, free, free_n = get_data( file_name )
    pos = occ + free

    # Cantidad de posiciones por fila
    row_n = {}
    for n in rows:
        row_n[n] = 0

    row_n ={}
    for n in rows:
        row_n[n] = occ_n[n] + free_n[n]

    # Cabecera
    dibujo = svgwrite.Drawing(file_name.replace(".xlsx","")+"-occupancy.svg", size=(5000, 5000), profile="full")
    s=50
    header(dibujo, 10, 200, file_name.replace(".xlsx","") )
    leyenda(dibujo, 10, 80) 

    # Funcion
    for n in rows:

        x=100
        s=100+s
        occp = int(100*round(occ_n[n]/row_n[n],2))
        texto(dibujo, str(occp)+"%", x-25 ,s+45, size=20, color="blue" )
        for m in range(0,row_n[n]):
            x=30+x
            y=30
            if str(str(n)+"-"+str(m)) in occ:
                rectangulo(dibujo, x, s+y, 30, 30, fill_color='gray', fill_opacity=1, line_color='black', line_opacity=1, style='full')
            rectangulo(dibujo, x, s+y, 30, 30, fill_color='white', fill_opacity=0, line_color='black', line_opacity=1, style='full')
            texto(dibujo, n+str(m), x+10,s+y+20, size=7 )
        dibujo.save()



##############################
#       MAIN
##############################
archivos = get_files()
for n in archivos:
    main(n)
