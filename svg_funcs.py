import time
import svgwrite
from svgwrite.image import Image
from svgwrite.text import TextPath


##############################
#       FUNCIONES
##############################
def texto(dwg, texto, x, y, color="black", size=12, bold=True, opacity=1):
    '''
    Funcion para crear texto.
    INPUT: *texto: texto a agregar al archivo SVG
           *x: coordenada x
           *y: coordenada y
           *color: color del texto
           *size: tamaño del texo en px
           *bold: True para texto en negrita.
           *opacity: opacidad de linea (0-1)
    '''
    texto1 = svgwrite.text.Text( texto, insert=(x,y), font_size=str(size)+'px' )
    if bold==True:  
        texto1 = svgwrite.text.Text( texto, insert=(x,y), font_size=str(size)+'px', font_weight='bold' )
    texto1.fill(color=color, rule=None, opacity=opacity)
    dwg.add( texto1 )


def linea(dwg, x1, y1, x2, y2, color="black", width=1, opacity=1, style='full' ):
    '''
    Funcion para crear líneas.
    INPUT: *x1: coordenada x origen
           *y1: coordenada y origen
           *x2: coordenada x destino
           *y2: coordenada y destino
           *color: color de linea
           *width: ancho de linea
           *opacity: opacidad de linea (0-1)
           *style: 'full' para linea continua, 'dotted' para linea de puntos, 'dashed' para linea de trazo
    '''
    linea1 = svgwrite.shapes.Line( start=(x1,y1), end=(x2,y2) )
    if style=='full':
        linea1.stroke(color, width, opacity)
    if style=='dotted':
        linea1.stroke(color, width, opacity).dasharray([2,2])
    if style=='dashed':
        linea1.stroke(color, width, opacity).dasharray([10,10])
    dwg.add( linea1 )


def rectangulo(dwg, x, y, length, width, fill_color='yellow', fill_opacity=1, line_color='black', line_opacity=1, style='full'):
    '''
    Funcion para crear rectangulos.
    INPUT: *x: coordenada x
           *y: coordenada y
           *height: alto del rectangulo
           *width: ancho del rectangulo
           *fill_color: color de relleno (black, yellow, green, blue...)
           *fill_opacity: opacidad del relleno (0-1)
           *line_color: color de linea
           *line_opacity: opacidad del opacity (0-1)
           *style: 'full' para linea continua, 'dotted' para linea de puntos, 'dashed' para linea de trazo
    '''
    rect1 = svgwrite.shapes.Rect( insert=(x,y), size=(length, width) )
    rect1.fill(color=fill_color, rule=None, opacity=fill_opacity)
    if style=='full':
        rect1.stroke(color=line_color, opacity=line_opacity)
    if style=='dotted':
        rect1.stroke(color=line_color, opacity=line_opacity).dasharray([2,2])
    if style=='dashed':
        rect1.stroke(color=line_color, opacity=line_opacity).dasharray([10,10])
    dwg.add( rect1 )


def poligono(dwg, points, fill_color='yellow', fill_opacity=1, line_color='black', line_opacity=1, style='full'):
    '''
    Funcion para crear polígonos.
    INPUT: *points: lista de tuplas con coordenadas de los puntos. Ej: [ (0,0), (1,0), (1,1), (0,1) ]
           *fill_color: color de relleno (black, yellow, green, blue...)
           *fill_opacity: opacidad del relleno (0-1)
           *line_color: color de linea
           *line_opacity: opacidad del opacity (0-1)
           *style: 'full' para linea continua, 'dotted' para linea de puntos, 'dashed' para linea de trazo
    '''
    polig1 = svgwrite.shapes.Polygon( points=points )
    polig1.fill(color=fill_color, rule=None, opacity=fill_opacity)
    if style=='full':
        polig1.stroke(color=line_color, opacity=line_opacity)
    if style=='dotted':
        polig1.stroke(color=line_color, opacity=line_opacity).dasharray([2,2])
    if style=='dashed':
        polig1.stroke(color=line_color, opacity=line_opacity).dasharray([10,10])
    dwg.add( polig1 )


def imagen(dwg, file, x, y, size):
    '''
    Funcion para agregar imagenes al SVG.
    INPUT:  *file: archivo de la imagen a agregar
            *x: coordenada x
            *y: coordenada y
            *size: tamaño en pixeles. Conserva relacion de aspecto
    '''
    img1 = svgwrite.image.Image( file, insert=(x,y), size=(size,size) )
    img1.fit(horiz='center', vert='middle', scale='meet')
    dwg.add( img1 )


def siteA( dwg, x, y, A, Ae, Ab, Ap):
    '''
    Funcion para generar el Sitio A
    INPUT:  *x: coordenada x
            *y: coordenada y
    '''
    s=40
    # Sitio
    rectangulo( dwg, x,y, 200, 250, fill_opacity=0, style="dotted" )
    texto( dwg, A, x, y-5, color="black", size=16, bold=True, opacity=1)

    # Placa tributaria
    rectangulo( dwg, x+120,y+75, 25,120 )
    texto( dwg, Ab, x+120, y+210, color="black", size=11, bold=False, opacity=1)
    texto( dwg, Ap, x+120, y+220, color="black", size=11, bold=False, opacity=1)

    # Placa de linea
    poligono( dwg, points=[(s+x+120,y+75), (s+x+145,y+20+75), (s+x+145,y+175), (s+x+120,y+195)] )
    texto( dwg, "LINE", x+160, y+210, color="black", size=11, bold=False, opacity=1)
    texto( dwg, "", x+160, y+220, color="black", size=11, bold=False, opacity=1)
    
    # Equipo
    texto( dwg, Ae, x+120, y+50, color="black", size=14, bold=True, opacity=1)

    # Router
    imagen( dwg, "r2.png", x+10, y+130, 60)

    # Conexion Router-Trib
    linea( dwg, x+70, y+160, x+120, y+160, color='blue')


def siteB( dwg, x, y, B, Be, Bb, Bp):
    '''
    Funcion para generar el Sitio B
    INPUT:  *x: coordenada x
            *y: coordenada y
    '''
    s=40
    # Sitio
    rectangulo( dwg, x,y, 200, 250, fill_opacity=0, style="dotted" )
    texto( dwg, B, x, y-5, color="black", size=16, bold=True, opacity=1)

    # Placa tributaria
    rectangulo( dwg, x+60,y+75, 25,120 )
    texto( dwg, Bb, x+60, y+210, color="black", size=11, bold=False, opacity=1)
    texto( dwg, Bp, x+60, y+220, color="black", size=11, bold=False, opacity=1)

    # Placa de linea
    poligono( dwg, points=[(s+x+5,y+75), (s+x-20,y+20+75), (s+x-20,y+175), (s+x+5,y+195)] )
    texto( dwg, "LINE", x+20, y+210, color="black", size=11, bold=False, opacity=1)
    texto( dwg, "", x+20, y+220, color="black", size=11, bold=False, opacity=1)
    
    # Equipo
    texto( dwg, Be, x+20, y+50, color="black", size=14, bold=True, opacity=1)

    # Router
    imagen( dwg, "r2.png", x+130, y+130, 60)

    # Conexion Router-Trib
    linea( dwg, x+85, y+160, x+130, y+160, color='blue')


def linkAB( dwg, x, y, sitioA, sitioB, equipA, equipB, boardA, portA, boardB, portB, path):
    '''
    Funcion para generar link entre Sitio A - Sitio B
    INPUT:  *x: coordenada x
            *y: coordenada y
    '''
    siteA( dibujo, x,y, sitioA, equipA, boardA, portA)
    siteB( dibujo, x+600, y, sitioB, equipB, boardB, portB)
    linea( dibujo, x+185, y+130, x+620,y+130, color="purple")
    texto( dibujo, path, x+330, y+125, )


def header( dwg, x, y, serv_name):
    # Cabecera
    año = time.strftime("%Y")
    texto( dwg, "PLANO DE OCUPACION DE SITIO", x+280, y-160, size=22, bold=True, color='black')
    texto( dwg, "INFRAESTRUCTURA - "+str(año), x, y-160, size=12, bold=False, color='black')
    linea( dwg, x, y-150, x+800,y-150)
    imagen( dwg, "Claro.png", x+700, y-225, 100)
    texto( dwg, serv_name, x+330, y-100, size=24, bold=True, color='green')

    # Pie
    #texto( dwg, time.strftime("%d/%m/%Y"), x, y+370, size=12, bold=False, color='black')
    #linea( dwg, x, y+350, x+800,y+350)


def leyenda(dwg, x,y):
    rectangulo(dwg, x, y, 20, 20, fill_color='gray', fill_opacity=1, line_color='black', line_opacity=1, style='full')
    rectangulo(dwg, x, y+30, 20, 20, fill_color='white', fill_opacity=1, line_color='black', line_opacity=1, style='full')
    texto( dwg, "Cabinet ocupado", x+30, y+15, size=12, bold=False, color='black')
    texto( dwg, "Cabinet libre", x+30, y+45, size=12, bold=False, color='black')
